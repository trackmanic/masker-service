# Masker-service

Makser service intended to hide original url and substitute it for
another one to make seem that this is another resource.

## Install dependencies

`go get -v`

## Build and run

`cd cmd && go build && ./cmd --config=config.toml`

## API

Masker serves two separate type of entities - codes and images.
It stores mapping of masked code url to original one and the same for
images.

List all codes templates `GET /api/code`
List all image templates `GET /api/image`

Get particular code template `GET /api/code/template/{id}`
Get particular image template `GET /api/image/template/{id}`


Create particular code url by template `POST /api/code`
Create particular image url by template `POST /api/image`

Example body for creating code

```
[
    {
        "original_url": "code_url_1",
        "tracking_domain_origin_id": 8,
        "template_id": "1"
    },
    {
        "original_url": "code_url_2",
        "tracking_domain_origin_id": 9,
        "template_id": "2"
    }
]
```

Example body for creating image

```
[
    {
        "original_url": "image_url_1",
        "tracking_domain_origin_id": 8,
        "template_id": "5"
    },
    {
        "original_url": "image_url_2",
        "tracking_domain_origin_id": 9,
        "template_id": "3"
    }
]
```


Delete particular mask `DELETE /api/mask`

Example body for creating code

```
[
    {
        "original_url": "code_url_1",
        "tracking_domain_origin_id": 8
    },
    {
        "original_url": "code_url_2",
        "tracking_domain_origin_id": 9        
    }
]
```

Example response

```
[
    {
    	"id": 13,
    	"masked_code": "revive/www/delivery/spcjs.php?id=7507943632"
    },
    {
        "id": 14,
        "masked_code": "revive/www/delivery/spcjs.php?id=9507938561"
    }
]
```

Get all tracking domains `GET /api/tracking_domain`

Create new tracking domain

```
{
    "domain": "mydomain.com",
    "origin_id": 1234
}
```

response


```
{
    "id": 1,
    "domain": "mydomain.com",
    "origin_id": 1234
}
```

Delete Tracking domain `GET /api/tracking_domain/{origin_id}`

Update tracking domain

```
{
    "domain": "newdomain.com",
    "origin_id": 1234
}
```
