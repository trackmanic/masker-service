package masker_service

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strings"
	"sync"
)

var (
	templateOnce      sync.Once
	templaterInstance *Templater
)

type Template struct {
	Id          string `json:"id"`
	Description string `json:"description"`
	Template    string `json:"template"`
	Template2   string `json:"template2"`
}

type Templater struct {
	codeTemplates  sync.Map
	imageTemplates sync.Map
}

func InitTemplater(config *TomlConfig) {
	GetLogger().Infof("Read templateMap from %s", config.Main.TemplateDir)

	GetTemplater().readFiles(config.Main.TemplateDir)
}

func (t *Templater) readFiles(dir string) {
	files, _ := ioutil.ReadDir(dir)

	if len(files) < 1 {
		GetLogger().Warnf("There are no templateMap")
		return
	}

	for _, file := range files {
		fileName := file.Name()
		filePath := path.Join(dir, fileName)

		if !file.IsDir() {
			GetLogger().Infof("parsing %s", filePath)

			if file, err := os.Open(filePath); err == nil {
				if data, err := ioutil.ReadAll(file); err == nil {
					tmpl := Template{}

					if err := json.Unmarshal(data, &tmpl); err == nil {
						if strings.Contains(dir, "codes") {
							t.codeTemplates.Store(tmpl.Id, tmpl)
						} else {
							t.imageTemplates.Store(tmpl.Id, tmpl)
						}
					} else {
						GetLogger().Error(filePath, err)
					}
				} else {
					GetLogger().Error(filePath, err)
				}
			} else {
				GetLogger().Error(filePath, err)
			}
		} else {
			t.readFiles(path.Join(dir, file.Name()))
		}
	}
}

func (t *Templater) listCodes(w http.ResponseWriter) {
	codeTemplates := make(map[string]TemplateView)

	t.codeTemplates.Range(func(key, value interface{}) bool {
		fileName, _ := key.(string)
		t, ok := value.(Template)

		if !ok {
			GetLogger().Error("Cannot convert value to Template")
		}

		view := TemplateView{}
		view.Template = t
		view.Actions = GetActions("code", t.Id)

		codeTemplates[fileName] = view

		return true
	})

	if data, err := json.Marshal(codeTemplates); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else {
		var buffer bytes.Buffer
		json.Indent(&buffer, data, "", "\t")

		w.Write(buffer.Bytes())
	}
}

func (t *Templater) listImages(w http.ResponseWriter) {
	codeTemplates := make(map[string]TemplateView)

	t.imageTemplates.Range(func(key, value interface{}) bool {
		fileName, _ := key.(string)
		t, ok := value.(Template)

		if !ok {
			GetLogger().Error("Cannot convert value to Template")
		}

		view := TemplateView{}
		view.Template = t
		view.Actions = GetActions("image", t.Id)

		codeTemplates[fileName] = view

		return true
	})

	if data, err := json.Marshal(codeTemplates); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	} else {
		var buffer bytes.Buffer
		json.Indent(&buffer, data, "", "\t")

		w.Write(buffer.Bytes())
	}
}

func GetTemplater() *Templater {
	templateOnce.Do(func() {
		templaterInstance = &Templater{
			codeTemplates:  sync.Map{},
			imageTemplates: sync.Map{},
		}
	})

	return templaterInstance
}
