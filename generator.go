package masker_service

import (
	"bytes"
	"fmt"
	"math/rand"
)

var sizes = []string{"300x250", "320x50", "728x90", "160x600"}

func Generate(template, template2 string) (string, string) {
	var b bytes.Buffer
	var b2 bytes.Buffer
	var tmpSize string

	substitutes := make([]byte, 0, len(template))

	for _, c := range template {
		if c == '*' {
			var s string
			num := byte('0' + rand.Int()%10)
			s = string(num)
			substitutes = append(substitutes, s[0])
			fmt.Fprint(&b, s)
		} else if c == '#' {
			var s string

			num := rand.Int() % 36

			if num < 10 {
				s = string('0' + num)
			} else {
				s = string('a' + num - 10)
			}

			substitutes = append(substitutes, s[0])
			fmt.Fprint(&b, s)
		} else if c == '@' {
			var s string

			num := rand.Int() % 3

			switch num {
			case 0:
				num = rand.Int() % 10
				s = string('0' + num)
			case 1:
				num = rand.Int() % 26
				s = string('a' + num)
			case 2:
				num = rand.Int() % 26
				s = string('A' + num)
			}

			substitutes = append(substitutes, s[0])
			fmt.Fprint(&b, s)
		} else if c == '$' {
			var s string

			num := rand.Int() % 26
			s = string('a' + num)

			substitutes = append(substitutes, s[0])
			fmt.Fprint(&b, s)
		} else if c == '%' {
			index := rand.Int() % len(sizes)
			size := sizes[index]
			tmpSize = size
			fmt.Fprint(&b, size)
		} else {
			fmt.Fprint(&b, string(c))
		}
	}

	i := 0

	for _, c := range template2 {
		if c == '*' || c == '#' || c == '@' || c == '$' {
			fmt.Fprint(&b2, string(substitutes[i]))
			i++
		} else if c == '%' {
			fmt.Fprint(&b2, tmpSize)
		}else {
			fmt.Fprint(&b2, string(c))
		}
	}

	return b.String(), b2.String()
}
