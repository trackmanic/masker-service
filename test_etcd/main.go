package main

func main() {
	cfg := client.Config{Endpoints: []string{"http://etcd1:2379", "http://etcd2:2379", "http://etcd3:2379"}}
	c, err := client.New(cfg)
	if err != nil {
		log.Fatal(err)
	}

	kapi := client.NewKeysAPI(c)
	resp, err := kapi.Set(ctx, "test", "bar", nil)
	if err != nil {
		if err == context.Canceled {
			// ctx is canceled by another routine
		} else if err == context.DeadlineExceeded {
			// ctx is attached with a deadline and it exceeded
		} else if cerr, ok := err.(*client.ClusterError); ok {
			// process (cerr.Errors)
		} else {
			// bad cluster endpoints, which are not etcd servers
		}
	}
}
