package masker_service

import "github.com/sirupsen/logrus"

var logger *logrus.Logger

func init() {
	logger = logrus.New()
}

func InitLogger(config *TomlConfig) {
	logger = logrus.New()
}

func GetLogger() *logrus.Logger {
	return logger
}
