package main

import (
	"fmt"
	"log"
	. "masker-service"
	"os"
	"text/template"
)

func main() {
	tpl := "/*.0/****/*******/*/1*0/ADTECH"
	fileNameTemplate := "test_%d"

	sizes := []int{10, 100, 1000, 10000, 15000, 25000, 50000}

	for _, size := range sizes {
		masks := make([]string, 0)
		log.Printf("Generate template for %d masks", size)

		for i := 0; i < size; i++ {
			masks = append(masks, Generate(tpl))
		}

		fileName := fmt.Sprintf(fileNameTemplate, size)
		log.Printf("Write to file %s", fileName)

		file, err := os.Create(fileName)

		if err != nil {
			panic(err)
		}

		t, err := template.ParseFiles("load_test/nginx.tpl")

		if err != nil {
			panic(err)
		}

		err = t.Execute(file, masks)

		if err != nil {
			panic(err)
		}

		file.Close()
	}
}
