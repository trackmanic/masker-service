server {
	listen 80 default_server;
	listen [::]:80 default_server;

	server_name _;

	  	location / {
	    {{range . }}
			location = {{ . }} {
  				return http://cloud.imagenes-7.com/26z2jmde9mhZwNI97RNi;
  			}

	    {{end}}
		}
}