package masker_service

import (
	"fmt"
	"net/http"
)

func GetActions(object, id string) []Action {
	return []Action{
		{
			Description: "List all",
			Method:      http.MethodGet,
			Url:         "/api/code",
		},
		{
			Description: "Show one",
			Method:      http.MethodGet,
			Url:         fmt.Sprintf("/api/%s/%s", object, id),
		},
		{
			Description: "Create",
			Method:      http.MethodPost,
			Url:         fmt.Sprintf("/api/%s/template/%s", object, id),
		},
		{
			Description: "Update",
			Method:      http.MethodPut,
			Url:         fmt.Sprintf("/api/%s/%s", object, id),
		},

		{
			Description: "Delete",
			Method:      http.MethodDelete,
			Url:         fmt.Sprintf("/api/%s/%s", object, id),
		},
	}
}

// TODO(stgleb): Consider exceptions in copying headers https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers#hbh
func CopyHeaders(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}
