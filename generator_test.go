package masker_service

import (
	"regexp"
	"testing"
)

func TestGenerate(t *testing.T) {
	testData := []struct {
		Template string
		Regexp   string
		Template2 string
	}{
		{
			"/adserver/;ID=******;size=1**x1**;setID=******;type=async;domid=placement_******_0;place=0;pid=*******;sw=10**;sh=10**;spr=1;rnd=*******",
			"adserver/;ID=[0-9]{6};size=[1-9][0-9]{2}x[1-9][0-9]{2};setID=[0-9]{6};type=async;domid=placement_[0-9]{6}_0;place=0;pid=[0-9]{7};sw=10[0-9]{2};sh=10[0-9]{2};spr=1;rnd=[0-9]{7}",
			"",
		},
		{
			"/*.0/****/*******/*/1*0/ADTECH",
			"/[0-9]{1}.0/[0-9]{4}/[0-9]{7}/[0-9]{1}/1[0-9]0/ADTECH",
			"",
		},
		{
			"v/1.*/av?auid=**********",
			"v/[1-2]{1}.[0-9]{1}/av\\?auid=[0-9]{10}",
			"",
		},
		{
			"revive/www/delivery/spcjs.php?id=**********",
			"revive/www/delivery/spcjs.php\\?id=[0-9]{10}",
			"",
		},
		{
			"zone/############",
			"zone/[a-z0-9]{12}",
			"",
		},
		{
			"st?ad_type=javascript&ad_size=1**x1**&section=******&m6li=******",
			"st\\?ad_type=javascript&ad_size=[1-9][0-9]{2}x[1-9][0-9]{2}&section=[0-9]{6}&m6li=[0-9]{6}",
			"",
		},
		{
			"getad.img/;libID=******",
			"getad.img/;libID=[0-9]{6}",
			"",
		},
		{
			"***\"/Ad\"1*******\"St\"1**\"S\"$\"\"1**\"Sq\"109\"1*****\"Id1.jpg",
			"[0-9]{3}\"/Ad\"1[0-9]{7}\"St\"1[0-9]{2}\"S\"[a-z]{1}\"\"1[0-9]{2}\"Sq\"109\"1[0-9]{5}\"Id1.jpg",
			"",
		},
		{
			"banners/********.jpg",
			"banners/[0-9]{8}.jpg",
			"",
		},
		{
			"ad/############.jpg",
			"ad/[a-z0-9]{12}.jpg",
			"",
		},
		{
			"ad/############.jpg",
			"ad/[a-z0-9]{12}.jpg",
			"",
		},
		{
			"@@@@@@@@@@@@@@@@.jpg",
			"[a-zA-Z0-9]{16}.jpg",
			"",
		},
		{
			"******/***x***.jpg",
			"[0-9]{6}/[0-9]{3}x[0-9]{3}.jpg",
			"",
		},
		{
			"/st?ad_type=javascript&ad_size=%&section=******&m6li=******",
			"st\\?ad_type=javascript&ad_size=[1-9][0-9]{2}x[1-9][0-9]{1,2}&section=[0-9]{6}&m6li=[0-9]{6}",
			"/tsm32/ad_type-javascript/ad_size-%/section=******/m6li-******",
		},
	}

	for _, test := range testData {
		mask, _ := Generate(test.Template, test.Template2)

		if ok, err := regexp.MatchString(test.Regexp, mask); !ok || err != nil {
			t.Errorf("Mask doesnt match pattern \n %s \n%s", mask, test.Regexp)
		}
	}
}
