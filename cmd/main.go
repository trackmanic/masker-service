package main

import (
	"flag"
	. "masker-service"
	"time"
)

var (
	configFileName string
)

func init() {
	flag.StringVar(&configFileName, "config", "config.toml", "config file name")
	flag.Parse()
}

func main() {
	config, err := NewConfig(configFileName)

	if err != nil {
		GetLogger().Fatal(err)
	}

	masker := Init(config)
	err = InitEtcD(config.Etcd.UserName, config.Etcd.Password,
		config.Etcd.Hosts, time.Duration(config.Etcd.Timeout)*time.Second)

	if err != nil {
		GetLogger().Fatalf("Error while connecting to Etcd %s", err)
	}

	masker.Run(config.Main.ListenStr)
}
