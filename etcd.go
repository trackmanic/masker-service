package masker_service

import (
	"context"
	"fmt"
	"github.com/coreos/etcd/client"
	"time"
)

var (
	etcdClient client.Client
)

func InitEtcD(userName, password string, etcdHosts []string, timeout time.Duration) error {
	cfg := client.Config{
		Endpoints:               etcdHosts,
		Transport:               client.DefaultTransport,
		HeaderTimeoutPerRequest: timeout,
		Username:                userName,
		Password:                password,
	}

	c, err := client.New(cfg)

	if err != nil {
		return err
	}

	etcdClient = c

	return nil
}

func PutMask(environment, trackingDomain, mask, maskValue string) error {
	kapi := client.NewKeysAPI(etcdClient)

	// Escape original url because it etcd treat slash as a key separator
	key := fmt.Sprintf("masker/%s/%s/%s", environment, trackingDomain, mask)

	resp, err := kapi.Create(context.Background(),
		key, maskValue)
	GetLogger().Println("Action was made %s", resp)

	return err
}

func DeleteMask(environment, trackingDomain, maskKey string) error {
	kapi := client.NewKeysAPI(etcdClient)

	// Escape original url because it etcd treat slash as a key separator
	key := fmt.Sprintf("masker/%s/%s/%s", environment, trackingDomain, maskKey)

	deleteOptions := &client.DeleteOptions{}

	resp, err := kapi.Delete(context.Background(), key, deleteOptions)
	GetLogger().Println("Action was made %s", resp)

	return err
}
