package masker_service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"io/ioutil"
	"masker-service/storage"
	"net/http"
	"strconv"
)

type Action struct {
	Description string `json:"description"`
	Method      string `json:"method"`
	Url         string `json:"url"`
}

type TemplateView struct {
	Template
	Actions []Action `json:"actions"`
}

type Original struct {
	OriginalURL    string `json:"original_url"`
	TemplateId     string `json:"template_id"`
	TrackingDomain int64  `json:"tracking_domain_origin_id"`
}

type MaskedCode struct {
	Id          int64  `json:"id"`
	Key         string `json:"key"`
	MaskedCode  string `json:"masked_code"`
	OriginalURL string `json:"original_url"`
	Cache       string `json:"cache"`
}

type DeleteRequest struct {
	OriginalURL    string `json:"original_url"`
	TrackingDomain int64  `json:"tracking_domain_origin_id"`
}

func (masker *Masker) ListCodeTemplates(w http.ResponseWriter, r *http.Request) {
	GetLogger().Infof("List code templates")
	GetTemplater().listCodes(w)
}

func (masker *Masker) ListImageTemplates(w http.ResponseWriter, r *http.Request) {
	GetLogger().Infof("List image templates")
	GetTemplater().listImages(w)
}

func (masker *Masker) ShowCodeTemplate(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	if val, ok := GetTemplater().codeTemplates.Load(id); ok {
		if t, ok := val.(Template); ok {
			view := TemplateView{}
			view.Template = t
			view.Actions = GetActions("code", t.Id)

			if data, err := json.Marshal(view); err == nil {
				var buffer bytes.Buffer
				json.Indent(&buffer, data, "", "\t")

				w.Write(buffer.Bytes())
			}
		} else {
			http.Error(w, fmt.Sprintf("Can't find template for code %s", id), http.StatusNotFound)
		}
	}
}

func (masker *Masker) ShowImageTemplate(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	type OriginalCode struct {
		originalCode string `json:"original_code"`
	}

	if val, ok := GetTemplater().codeTemplates.Load(id); ok {
		if t, ok := val.(Template); ok {
			view := TemplateView{}
			view.Template = t
			view.Actions = GetActions("image", t.Id)

			if data, err := json.Marshal(view); err == nil {
				var buffer bytes.Buffer
				json.Indent(&buffer, data, "", "\t")

				w.Write(buffer.Bytes())
			}
		} else {
			http.Error(w, fmt.Sprintf("Can't find template for code %s", id), http.StatusNotFound)
		}
	}
}

func (masker *Masker) AddCode(w http.ResponseWriter, r *http.Request) {
	GetLogger().Println("Add code")
	masker.CreateMask(w, r, "false")
}

func (masker *Masker) AddImage(w http.ResponseWriter, r *http.Request) {
	GetLogger().Println("Add image")
	masker.CreateMask(w, r, "true")
}

func (masker *Masker) CreateMask(w http.ResponseWriter, r *http.Request, cache string) {
	var t Template

	originals := make([]Original, 0, 256)
	masks := make([]MaskedCode, 0, 256)

	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	if err != nil {
		http.Error(w, "", http.StatusBadRequest)
	}

	if err := json.Unmarshal(body, &originals); err == nil {
		for _, o := range originals {
			GetLogger().Infof("Create mapping for template %s", t.Id)

			if val, ok := GetTemplater().codeTemplates.Load(o.TemplateId); ok {
				t, ok = val.(Template)

				if !ok {
					http.Error(w, fmt.Sprintf("Can't convert map value to template %v", val),
						http.StatusInternalServerError)
					return
				}
			} else {
				http.Error(w, fmt.Sprintf("Can't find template for code %s", o.TemplateId), http.StatusNotFound)
				return
			}

			maskedURL, maskedURL2 := Generate(t.Template, t.Template2)

			GetLogger().Infof("Generated url %s", maskedURL)

			maskId, err := storage.InsertMask(maskedURL, o.OriginalURL)

			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}

			trackingDomains, err := storage.GetTrackingDomainsById(o.TrackingDomain)

			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}

			if len(trackingDomains) == 0 {
				http.Error(w, fmt.Sprintf("Tracking domain with id %d not found", o.TrackingDomain), http.StatusNotFound)
				return
			}

			trck := trackingDomains[0]

			// Check limits of masks per tracking domain
			countByOriginId := storage.CountMaskByTrackingDomain(trck.Id)

			// TODO(stgleb): move to config
			if countByOriginId > 5000 {
				http.Error(w, "To many masks per domain", http.StatusBadRequest)
				return
			}

			// Establish relationship between mask and tracking domain
			storage.InsertMaskToTrackingDomain(maskId, trackingDomains[0].Id)

			tmp, err := uuid.NewV4()
			key := tmp.String()

			mask := MaskedCode{
				Id:          maskId,
				Key:         key,
				MaskedCode:  maskedURL,
				OriginalURL: fmt.Sprintf("https://%s/%s", trck.Domain, o.OriginalURL),
				Cache:       cache,
			}

			var maskData []byte

			if len(maskedURL2) > 0 {
				mask2 := MaskedCode{
					Id:          maskId,
					Key:         key,
					MaskedCode:  maskedURL2,
					OriginalURL: fmt.Sprintf("https://%s/%s", trck.Domain, o.OriginalURL),
					Cache:       cache,
				}

				maskData, err = json.Marshal(mask2)
			} else {
				maskData, err = json.Marshal(mask)
			}

			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			// Put entire mask data as a value to etcd
			err = PutMask(GetConfig().Main.Environment, trck.Domain, key, string(maskData))

			if err != nil {
				GetLogger().Errorf("Error while putting mapping to EtcD %s", err.Error())
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}

			masks = append(masks, mask)
		}

		if data, err := json.Marshal(masks); err == nil {
			w.WriteHeader(http.StatusCreated)
			var buffer bytes.Buffer
			json.Indent(&buffer, data, "", "\t")
			data = buffer.Bytes()
			w.Write(data)
		} else {
			http.Error(w, "", http.StatusInternalServerError)
		}
	} else {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
}

// TODO(stgleb): Implement method for restoring mask from database.
func (masker *Masker) DeleteMask(w http.ResponseWriter, r *http.Request) {
	originals := make([]DeleteRequest, 0, 256)
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	if err != nil {
		http.Error(w, "", http.StatusBadRequest)
	}

	if err := json.Unmarshal(body, &originals); err == nil {
		for _, o := range originals {
			trackingDomains, err := storage.GetTrackingDomainsById(o.TrackingDomain)

			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}

			if len(trackingDomains) == 0 {
				http.Error(w, fmt.Sprintf("Tracking domain with id %d not found", o.TrackingDomain), http.StatusNotFound)
				return
			}

			trck := trackingDomains[0]
			storage.SoftDeleteMaskByOriginalUrl(o.OriginalURL)

			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			DeleteMask(GetConfig().Main.Environment, trck.Domain, o.OriginalURL)
		}
	} else {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
}

func (masker *Masker) CreateTrackingDomain(w http.ResponseWriter, r *http.Request) {
	GetLogger().Infof("Create TrackingDomain")

	td := storage.TrackingDomain{}
	body, err := ioutil.ReadAll(r.Body)

	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := json.Unmarshal(body, &td); err == nil {
		if id, err := storage.InsertTrackingDomain(td); err == nil {
			w.WriteHeader(http.StatusCreated)
			td.Id = id
			json.NewEncoder(w).Encode(td)
		} else {
			GetLogger().Error(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	} else {
		GetLogger().Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
}

func (masker *Masker) GetTrackingDomainById(w http.ResponseWriter, r *http.Request) {
	GetLogger().Infof("Get TrackingDomains")
	// Get origin id from request
	originId, err := masker.getOriginId(r)

	if err != nil {
		http.Error(w, "Id must be integer", http.StatusBadRequest)
		return
	}

	if val, err := storage.GetTrackingDomainsById(originId); err == nil {
		if data, err := json.Marshal(val); err == nil {
			var buffer bytes.Buffer
			json.Indent(&buffer, data, "", "\t")
			w.Write(buffer.Bytes())
		} else {
			GetLogger().Debug(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	} else {
		err = errors.Wrap(err, fmt.Sprintf("Can't find TrackingDomain with %s", originId))
		GetLogger().Debug(err)
		http.Error(w, err.Error(), http.StatusNotFound)
	}
}

func (masker *Masker) GetAllTrackingDomains(w http.ResponseWriter, r *http.Request) {
	GetLogger().Infof("Get All TrackingDomains")

	if val, err := storage.GetAllTrackingDomains(); err == nil {
		if data, err := json.Marshal(val); err == nil {
			var buffer bytes.Buffer
			json.Indent(&buffer, data, "", "\t")
			w.Write(buffer.Bytes())
		} else {
			GetLogger().Debug(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	} else {
		err = errors.Wrap(err, fmt.Sprintf("Can't find any TrackingDomain"))
		GetLogger().Debug(err)
		http.Error(w, err.Error(), http.StatusNotFound)
	}
}

func (masker *Masker) UpdateTrackingDomain(w http.ResponseWriter, r *http.Request) {
	GetLogger().Infof("UpdateTrackingDomain")
	originId, err := masker.getOriginId(r)

	if err != nil {
		http.Error(w, "Id most be integer", http.StatusBadRequest)
		return
	}
	td := storage.TrackingDomain{}

	body, err := ioutil.ReadAll(r.Body)

	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := json.Unmarshal(body, &td); err == nil {
		td.OriginId = originId

		if err := storage.UpdateTrackingDomainByOriginId(td); err == nil {
			w.WriteHeader(http.StatusOK)
		} else {
			GetLogger().Debug(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	} else {
		GetLogger().Debug(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
}

func (masker *Masker) DeleteTrackingDomain(w http.ResponseWriter, r *http.Request) {
	GetLogger().Infof("Delete TrackingDomain")
	originId, err := masker.getOriginId(r)
	if err != nil {
		http.Error(w, "Id most be integer", http.StatusBadRequest)
		return
	}

	if err := storage.SoftDeleteTrackingDomainByOriginId(originId); err == nil {
		w.WriteHeader(http.StatusOK)
	} else {
		GetLogger().Debug(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (masker *Masker) getOriginId(r *http.Request) (int64, error) {
	var originId int
	vars := mux.Vars(r)
	id := vars["origin_id"]
	originId, err := strconv.Atoi(id)

	if err != nil {
		GetLogger().Debug(err)
		return -1, err
	}

	return int64(originId), nil
}
