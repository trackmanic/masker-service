package masker_service

import (
	"fmt"
	"github.com/gorilla/mux"
	reuse "github.com/jbenet/go-reuseport"
	"masker-service/storage"
	"math/rand"
	"net/http"
	"time"
)

type Masker struct {
	http.Server
}

func Init(config *TomlConfig) *Masker {
	rand.Seed(time.Now().UnixNano())
	InitLogger(config)
	InitTemplater(config)
	InitDB(config)

	router := mux.NewRouter()

	masker := &Masker{
		http.Server{
			ReadTimeout:  config.Server.ReadTimeout * time.Second,
			WriteTimeout: config.Server.WriteTimeout * time.Second,
			IdleTimeout:  config.Server.IdleTimeout * time.Second,
			Handler:      router,
		},
	}

	router.HandleFunc("/api/code", masker.ListCodeTemplates).Methods(http.MethodGet)
	router.HandleFunc("/api/image", masker.ListImageTemplates).Methods(http.MethodGet)

	router.HandleFunc("/api/code", masker.AddCode).Methods(http.MethodPost)
	router.HandleFunc("/api/image", masker.AddImage).Methods(http.MethodPost)

	router.HandleFunc("/api/mask", masker.DeleteMask).Methods(http.MethodDelete)

	router.HandleFunc("/api/code/template/{id}", masker.ShowCodeTemplate).Methods(http.MethodGet)
	router.HandleFunc("/api/image/template/{id}", masker.ShowCodeTemplate).Methods(http.MethodGet)

	router.HandleFunc("/api/tracking_domain", masker.CreateTrackingDomain).Methods(http.MethodPost)
	router.HandleFunc("/api/tracking_domain", masker.GetAllTrackingDomains).Methods(http.MethodGet)

	router.HandleFunc("/api/tracking_domain/{origin_id}", masker.GetTrackingDomainById).Methods(http.MethodGet)
	router.HandleFunc("/api/tracking_domain/{origin_id}", masker.UpdateTrackingDomain).Methods(http.MethodPut)
	router.HandleFunc("/api/tracking_domain/{origin_id}", masker.DeleteTrackingDomain).Methods(http.MethodDelete)

	return masker
}

func (masker *Masker) Run(listenStr string) {
	l, err := reuse.Listen("tcp4", listenStr)

	if err != nil {
		GetLogger().Fatal(err)
	}

	GetLogger().Infof("Start masker service on %s", listenStr)
	if err := masker.Serve(l); err != nil {
		GetLogger().Fatal(err)
	}
}

func InitDB(config *TomlConfig) {
	dsn := "%s:%s@tcp(%s:%s)/%s?charset=utf8"
	storage.Connect(fmt.Sprintf(dsn,
		config.Mysql.User,
		config.Mysql.Password,
		config.Mysql.Host,
		config.Mysql.Port,
		config.Mysql.Database))
}
