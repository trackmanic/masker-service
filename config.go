package masker_service

import (
	"github.com/BurntSushi/toml"
	"time"
)

var config_instance *TomlConfig

type TomlConfig struct {
	Main   MainConfig
	Server ServerConfig
	Mysql  MysqlConfig
	Etcd   EtcDConfig
}

type MainConfig struct {
	ListenStr   string
	TemplateDir string
	Release     string
	Environment string
	StorageType int
}

type ServerConfig struct {
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
	IdleTimeout  time.Duration
}

type MysqlConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	Database string
}

type EtcDConfig struct {
	Hosts    []string
	Timeout  int64
	UserName string
	Password string
}

func GetConfig() *TomlConfig {
	return config_instance
}

func NewConfig(file string) (*TomlConfig, error) {
	config_instance = &TomlConfig{}

	if _, err := toml.DecodeFile(file, config_instance); err != nil {
		return nil, err
	}

	return config_instance, nil
}
