package main

import (
	"context"
	"github.com/coreos/etcd/client"
	"time"
)

func main() {
	cfg := client.Config{
		Endpoints:               []string{"http://localhost:2379"},
		Transport:               client.DefaultTransport,
		HeaderTimeoutPerRequest: time.Second * 1,
	}

	c, err := client.New(cfg)

	if err != nil {
		panic(err)
	}

	kapi := client.NewKeysAPI(c)
	kapi.Create(context.Background(), "domain.com/adtech", "abcd")
}
