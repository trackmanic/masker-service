package storage

import "github.com/sirupsen/logrus"

var storageLogger *logrus.Logger

func init() {
	storageLogger = logrus.New()
}

func InitLogger(logger *logrus.Logger) {
	storageLogger = logger
}

func GetLogger() *logrus.Logger {
	return storageLogger
}
