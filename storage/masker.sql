SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `masker` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `masker`;

CREATE TABLE `mask` (
  `id` int(11) NOT NULL,
  `mask` varchar(255) NOT NULL,
  `origin_url` varchar(255) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `mask_to_tracking_domain` (
  `mask_id` int(11) NOT NULL,
  `tracking_domain_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tracking_domain` (
  `id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `origin_id` int(11) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `mask`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mask` (`mask`);

ALTER TABLE `mask_to_tracking_domain`
  ADD PRIMARY KEY (`mask_id`,`tracking_domain_id`),
  ADD KEY `mask_id` (`mask_id`),
  ADD KEY `tracking_domain_id` (`tracking_domain_id`);

ALTER TABLE `tracking_domain`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `mask`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `tracking_domain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `mask_to_tracking_domain`
  ADD CONSTRAINT `FK_TRACKING_DOMAIN` FOREIGN KEY (`tracking_domain_id`) REFERENCES `tracking_domain` (`id`),
  ADD CONSTRAINT `FK_MASK` FOREIGN KEY (`mask_id`) REFERENCES `mask` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;