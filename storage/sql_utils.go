package storage

import (
	"database/sql"
	"encoding/json"
	"github.com/jmoiron/sqlx"
	"time"
)

const (
	insertMask                         = `INSERT INTO mask (mask, origin_url) VALUES( ?, ? )`
	selectMaskById                     = `SELECT * FROM mask WHERE mask.id=?`
	insertTrackingDomain               = `INSERT INTO tracking_domain (url, origin_id) VALUES( ?, ? )`
	selectTrackingDomainByOriginId     = `SELECT * FROM tracking_domain WHERE tracking_domain.origin_id=?`
	selectAllTrackingDomain            = `SELECT * FROM tracking_domain`
	updateTrackingDomainByOriginId     = `UPDATE tracking_domain SET url=? WHERE tracking_domain.origin_id=?`
	softDeleteTrackingDomainByOriginId = `UPDATE tracking_domain SET deleted_at=? WHERE tracking_domain.origin_id=?`
	softDeleteMaskByOriginalUrl        = `UPDATE mask SET deleted_at=? WHERE mask.origin_url=?`
	insertMaskToTrackingDomain         = `INSERT INTO mask_to_tracking_domain (mask_id, tracking_domain_id) VALUES( ?, ? )`
	countMaskByTrackingDomain          = `SELECT COUNT(*) FROM mask_to_tracking_domain WHERE tracking_domain_id=?`
)

// Mask represents row from 'mask' table
type Mask struct {
	Id        int64  `db:"id"          json:"id"`
	Mask      string `db:"mask"        json:"mask"`
	OriginUrl string `db:"origin_url"  json:"original_url"`
}

//TrackingDomain represents row from 'tracking_domain' table
type TrackingDomain struct {
	Id        int64         `db:"id"         json:"id"`
	Domain    string        `db:"url"        json:"domain"`
	OriginId  int64         `db:"origin_id"  json:"origin_id"`
	DeletedAt sql.NullInt64 `db:"deleted_at" json:"deleted_at"`
}

//MaskToTrackingDomain represents row from 'mask_to_tracking_domain' table
type MaskToTrackingDomain struct {
	MaskId          int64 `db:"mask_id"`
	TracingDomainId int64 `db:"tracking_domain_id"`
}

func scanTrackingDomains(rows *sqlx.Rows) ([]*TrackingDomain, error) {
	tds := make([]*TrackingDomain, 0)
	for rows.Next() {
		td := new(TrackingDomain)
		err := rows.StructScan(td)
		if err != nil {
			return nil, err
		}
		tds = append(tds, td)
		GetLogger().Debugf("%#v", td)
	}
	return tds, nil
}

func InsertMask(mask, originUrl string) (int64, error) {
	db := getDatabase()
	result, err := db.Exec(insertMask, mask, originUrl)

	if err != nil {
		return 0, err
	}

	return result.LastInsertId()
}

func GetMaskById(id int64) (*Mask, error) {
	mask := new(Mask)
	err := getDatabase().Get(mask, selectMaskById, id)
	return mask, err
}

func (v *TrackingDomain) MarshalJSON() ([]byte, error) {
	type Alias TrackingDomain
	tmp := &struct {
		DeletedAt int64 `json:"deleted_at"`
		*Alias
	}{
		Alias: (*Alias)(v),
	}
	tmp.DeletedAt = v.DeletedAt.Int64
	return json.Marshal(tmp)
}

func GetAllTrackingDomains() ([]*TrackingDomain, error) {
	rows, err := getDatabase().Queryx(selectAllTrackingDomain)
	if rows != nil {
		defer rows.Close()
	}
	if err != nil {
		return nil, err
	}
	tds, err := scanTrackingDomains(rows)
	if err != nil {
		return nil, err
	}
	return tds, err
}

func InsertTrackingDomain(domain TrackingDomain) (int64, error) {
	r, err := getDatabase().Exec(insertTrackingDomain, domain.Domain, domain.OriginId)

	if err != nil {
		return 0, err
	}

	return r.LastInsertId()
}

func GetTrackingDomainsById(originId int64) ([]*TrackingDomain, error) {
	rows, err := getDatabase().Queryx(selectTrackingDomainByOriginId, originId)

	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		return nil, err
	}

	tds, err := scanTrackingDomains(rows)

	if err != nil {
		return nil, err
	}

	return tds, err
}

func UpdateTrackingDomainByOriginId(domain TrackingDomain) error {
	_, err := getDatabase().Exec(updateTrackingDomainByOriginId, domain.Domain, domain.OriginId)
	return err
}

func SoftDeleteTrackingDomainByOriginId(originId int64) error {
	_, err := getDatabase().Exec(softDeleteTrackingDomainByOriginId, time.Now().UTC().Unix(), originId)

	if err != nil {
		return err
	}

	return nil
}

func SoftDeleteMaskByOriginalUrl(originalUrl string) error {
	_, err := getDatabase().Exec(softDeleteMaskByOriginalUrl, time.Now().UTC().Unix(), originalUrl)

	return err
}

func InsertMaskToTrackingDomain(maskId, tracingDomainId int64) error {
	_, err := getDatabase().Exec(insertMaskToTrackingDomain, maskId, tracingDomainId)
	return err
}

func CountMaskByTrackingDomain(originId int64) int {
	var count int
	rows := db.QueryRow(countMaskByTrackingDomain, originId)
	rows.Scan(&count)
	return count
}
