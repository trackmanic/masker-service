package storage

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

var (
	db       *sqlx.DB
	mysqlDSN string
)

func Set(dbc *sqlx.DB) {
	db = dbc
}

func getDatabase() *sqlx.DB {

	if db == nil {
		GetLogger().Fatalln("There is no sql connection. Use 'Connect' OR 'Set' method.")
	}

	// Reconnect if ping fails
	if err := db.Ping(); err != nil {
		Connect(mysqlDSN)
	}

	return db
}

func Connect(dsn string) {
	var err error
	db, err = sqlx.Connect("mysql", dsn)

	if err != nil {
		GetLogger().Fatalln(err)
	}

	mysqlDSN = dsn
}
